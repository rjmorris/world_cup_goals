I'm not much of a soccer fan, but I will watch a few matches when the World Cup rolls around. In the handful of matches I watched during the 2019 Women's World Cup, it struck me that most goals were scored in the first half.

This observation led me to wonder whether a higher-scoring first half is a common occurrence. Perhaps teams shift to a more defensive strategy after gaining the lead, resulting in fewer goals afterwards. Of course, under this assumption, the trailing team would likely need to shift to a more aggressive strategy, which may offset any goal-reduction effect. But maybe defense is better than offense in soccer (which seems reasonable given the low scores), and maybe the team scoring first is more likely to be the better team, meaning their defensive strategy would have a better chance of prevailing. If so, it seems reasonable to hypothesize that fewer goals would be scored later in the game as the winning team shifts to a defensive strategy.

I decided to investigate this in a little more detail by examining the goal distribution in the entire 2019 Women's World Cup. This is far from a rigorous treatment of the subject. It's just a quick look using data that was easy to obtain. It also gave me a chance to reconnect with the Python charting library [plotnine](https://github.com/has2k1/plotnine).

# Source Data

To obtain the data, I consulted [Wikipedia's article on the 2019 Women's World Cup](https://en.wikipedia.org/wiki/2019_FIFA_Women%27s_World_Cup), which listed the results of every match, including when each goal was scored. I entered that information in a [CSV file](data/goals.csv).

For the purposes of this analysis, I treated stoppage-time goals like they occurred during the last minute of the half. For example, if a goal was scored during the first half's stoppage time, I assumed it was scored in the 45th minute.

# Results

I'll jump right to the question that sparked this exercise: Are more goals scored in the first half than the second?

![goals by period](images/period.png)

That's a definite "no"! So much for my hypothesis. But hold on, I was watching matches during the knockout stage, so maybe the goal distribution was different then than in the group stage.

![goals by period and stage](images/period_stage_facet.png)

There were a few more first half goals than second half goals during the knockout stage. I suppose that's a small consolation for my hypothesis, but the difference was small and the sample size was small, so there is little to no supporting evidence here.

Is there anything else interesting to look at in this data? We can be a little more granular than goals in each half by plotting the goals in each 15 minute interval:

![goals by 15-minute interval](images/minute15.png)

The beginning and the end of the second half saw the most goals. We can dig even further by plotting the goals in each 5 minute interval:

![goals by 5-minute interval](images/minute5.png)

So not only were a lot of goals scored in the first 15 minutes of the second half, but they were concentrated in the first 5 minutes. It would be interesting to see whether this observation generalizes to other competitions or leagues.

Maybe certain teams were more likely to score in one half than another:

![goals by period and team](images/period_team.png)

We see that England and Germany were first half teams, while the Netherlands, France, and Italy were second half teams. We can also see that the US scored the most goals by far, which shouldn't come as a surprise given that they won the tournament and scored a record-breaking 13 goals in a single match.

Finally, we see that many more goals were scored in the second half's stoppage time than the first:

![stoppage goals by period](images/period_stoppage.png)

Although these plots are mildly interesting snapshots of this particular World Cup, they shouldn't be construed as attempts to draw conclusions about soccer in general. A larger, representative sample of soccer matches would be necessary for that, along with appropriate confidence intervals to provide guidance about the potential range of the estimates.

# plotnine

One motivation for this project was to check out the Python plotting library [plotnine](https://github.com/has2k1/plotnine), which is designed to be a clone of the popular R charting library [ggplot2](https://ggplot2.tidyverse.org/index.html). I found a pretty helpful website authored by a former colleague that displays several chart types and compares how one would create them in various Python plotting libraries (including plotnine) and also in ggplot2: [Plotting for Exploratory Data Analysis](http://pythonplot.com/Python).

This post isn't intended to be a plotnine tutorial, so I won't get into many details, but I'll provide an example to demonstrate the flavor of the library. Here's the code I used to create the chart titled "Goals scored in each 15 minute interval":

```
import plotnine as p9

# Import the data and manipulate it for use with plotnine.
df = ...

p = (
    p9.ggplot(data=df)
    + p9.aes(x="bin15", fill="period")
    + p9.geom_bar()
    + p9.scale_x_discrete(drop=False)
    + p9.scale_fill_brewer(type="qual", palette="Paired")
    + p9.theme(legend_title=p9.element_blank())
    + p9.xlab("minute")
    + p9.ylab("goals")
    + p9.ggtitle("Goals scored in each 15 minute interval")
)
p.save(filename="images/minute15.png")
```

A few notes on this example:

- Almost all examples and tutorials online do `from plotnine import *`, but I've (perhaps stubbornly) avoided that here because `import *` is typically considered a bad practice. Thus my example has `p9.` littered everywhere, but examples online don't.
- `p9.aes()` assigns the chart's "aesthetics", which refers to things like which variables go on the x- and y-axes, which variable controls the fill color, which variable controls the transparency, etc.
- `p9.geom_bar()` adds a bar chart to the plot.
- I used `p9.scale_x_discrete(drop=False)` to force plotnine to place _all_ 15 minute intervals on the x-axis, not just the ones in which goals were scored. (It turns out that goals were scored in every 15-minute interval, so adding this function to the plot wasn't strictly necessary. It was necessary in other plots, however, so I added it to this one for consistency.)
- I used `p9.scale_fill_brewer()` to specify the colors I wanted to use for the bars. "Brewer" in this context refers to the [ColorBrewer 2.0](http://colorbrewer2.org) palettes. I chose one that is color-blind friendly.
- By default, the legend includes a title, but I didn't think that was necessary here, so I deleted in `p9.theme()`.

In my experience, the documentation for plotnine wasn't as helpful as it could have been. In fact, I found that it was often easier to consult R's ggplot2 documentation and then translate that over to my Python code. To plotnine's credit, translating the ggplot2 code to plotnine code was seamless in most cases. One exception I ran into was when trying to remove the legend title (as mentioned in my example). ggplot2 supported a couple ways to do it that didn't work in plotnine, such as passing `name=p9.element_blank()` as an argument to `p9.scale_fill_brewer()`. Another gotcha when translating ggplot2 examples to plotnine was that ggplot2 examples sometimes used R functions and data structures, so I also needed to translate the R code to Python/pandas/numpy code.
