import math
import pandas as pd
from pathlib import Path
import plotnine as p9

data_dir = Path("./data")
image_dir = Path("./images")
image_dir.mkdir(exist_ok=True, parents=True)

df = pd.read_csv(data_dir / "goals.csv")

df["stage"] = df["knockout"].map({0: "group stage", 1: "knockout stage"})
df["stage"] = pd.Categorical(df["stage"], categories=["group stage", "knockout stage"])


def period(minute):
    if minute <= 45:
        return "first half"
    if minute <= 90:
        return "second half"
    if minute <= 120:
        return "extra time"
    raise ValueError(f"Unexpected minute: {minute}")


df["period"] = df["minute"].map(period)
df["period"] = pd.Categorical(df["period"], categories=["first half", "second half", "extra time"])


def assign_bin(width):
    def assign(minute):
        return math.ceil(minute / width)

    return assign


def assign_bin_label(width):
    def assign(bin_index):
        return "{}-{}".format((width * (bin_index - 1)) + 1, width * bin_index)

    return assign


for bin_width in (5, 10, 15):
    col = f"bin{bin_width}"
    binner = assign_bin(bin_width)
    labeler = assign_bin_label(bin_width)
    num_bins = int(120 / bin_width)

    df[col] = df["minute"].map(binner)
    df[col] = df[col].map(labeler)
    df[col] = pd.Categorical(
        values=df[col], categories=[labeler(i + 1) for i in range(num_bins)]
    )

p = (
    p9.ggplot(data=df)
    + p9.aes(x="period", fill="period")
    + p9.geom_bar()
    + p9.scale_x_discrete(drop=False)
    + p9.scale_fill_brewer(type="qual", palette="Paired")
    + p9.guides(fill=None)
    + p9.xlab("")
    + p9.ylab("goals")
    + p9.ggtitle("Goals scored in each period")
)
p.save(filename="images/period.png")

p = (
    p9.ggplot(data=df)
    + p9.aes(x="stage", fill="period")
    + p9.geom_bar(position=p9.position_dodge(preserve="single"))
    + p9.scale_x_discrete(drop=False)
    + p9.scale_fill_brewer(type="qual", palette="Paired", drop=False)
    + p9.theme(legend_title=p9.element_blank())
    + p9.xlab("")
    + p9.ylab("goals")
    + p9.ggtitle("Goals scored in each period, by stage")
)
p.save(filename="images/period_stage.png")

p = (
    p9.ggplot(data=df)
    + p9.aes(x="period", fill="period")
    + p9.geom_bar()
    + p9.facet_grid(facets=[".", "stage"], drop=False)
    + p9.scale_x_discrete(drop=False)
    + p9.scale_fill_brewer(type="qual", palette="Paired")
    + p9.guides(fill=None)
    + p9.xlab("")
    + p9.ylab("goals")
    + p9.ggtitle("Goals scored in each period, by stage")
)
p.save(filename="images/period_stage_facet.png")

p = (
    p9.ggplot(data=df)
    + p9.aes(x="bin15", fill="period")
    + p9.geom_bar()
    + p9.scale_x_discrete(drop=False)
    + p9.scale_fill_brewer(type="qual", palette="Paired")
    + p9.theme(legend_title=p9.element_blank())
    + p9.xlab("minute")
    + p9.ylab("goals")
    + p9.ggtitle("Goals scored in each 15 minute interval")
)
p.save(filename="images/minute15.png")

p = (
    p9.ggplot(data=df)
    + p9.aes(x="bin10", fill="period")
    + p9.geom_bar()
    + p9.scale_x_discrete(drop=False)
    + p9.scale_fill_brewer(type="qual", palette="Paired")
    + p9.theme(
        axis_text_x=p9.element_text(rotation=75),
        legend_title=p9.element_blank()
    )
    + p9.xlab("minute")
    + p9.ylab("goals")
    + p9.ggtitle("Goals scored in each 10 minute interval")
)
p.save(filename="images/minute10.png")

p = (
    p9.ggplot(data=df)
    + p9.aes(x="bin5", fill="period")
    + p9.geom_bar()
    + p9.scale_x_discrete(drop=False)
    + p9.scale_fill_brewer(type="qual", palette="Paired")
    + p9.theme(
        axis_text_x=p9.element_text(rotation=75),
        legend_title=p9.element_blank()
    )
    + p9.xlab("minute")
    + p9.ylab("goals")
    + p9.ggtitle("Goals scored in each 5 minute interval")
)
p.save(filename="images/minute5.png")

p = (
    p9.ggplot(data=df)
    + p9.aes(x="minute", fill="period")
    + p9.geom_bar()
    + p9.scale_fill_brewer(type="qual", palette="Paired")
    + p9.theme(legend_title=p9.element_blank())
    + p9.xlab("minute")
    + p9.ylab("goals")
    + p9.ggtitle("Goals scored in each minute")
)
p.save(filename="images/minute1.png")

# For this one, plotnine puts everything in the reverse order of how I want it.
# To work around this, first reverse several things so that they come out right
# after plotnine reverses them.
team_order = list(reversed(df["team"].value_counts().index))
orig_periods = list(df["period"].cat.categories)
df["period2"] = df["period"].cat.reorder_categories(list(reversed(orig_periods)))
colors = list(
    reversed(
        p9.scale_fill_brewer(type="qual", palette="Paired").palette(len(orig_periods))
    )
)
p = (
    p9.ggplot(data=df)
    + p9.aes(x="team", fill="period2")
    + p9.geom_bar()
    + p9.scale_x_discrete(drop=False, limits=team_order)
    + p9.scale_fill_manual(values=colors)
    + p9.guides(fill=p9.guide_legend(reverse=True))
    + p9.theme(legend_title=p9.element_blank())
    + p9.xlab("")
    + p9.ylab("goals")
    + p9.coord_flip()
    + p9.ggtitle("Goals scored in each period, by team")
)
p.save(filename="images/period_team.png")

p = (
    p9.ggplot(data=df[df["penalty"] == 1])
    + p9.aes(x="period", fill="period")
    + p9.geom_bar()
    + p9.scale_x_discrete(drop=False)
    + p9.scale_fill_brewer(type="qual", palette="Paired")
    + p9.guides(fill=None)
    + p9.xlab("")
    + p9.ylab("goals")
    + p9.ggtitle("Penalty goals scored in each period")
)
p.save(filename="images/period_penalty.png")

p = (
    p9.ggplot(data=df[pd.notnull(df["stoppage"])])
    + p9.aes(x="period", fill="period")
    + p9.geom_bar()
    + p9.scale_x_discrete(drop=False)
    + p9.scale_fill_brewer(type="qual", palette="Paired")
    + p9.guides(fill=None)
    + p9.xlab("")
    + p9.ylab("goals")
    + p9.ggtitle("Stoppage-time goals scored in each period")
)
p.save(filename="images/period_stoppage.png")
